// Custom JS 

/*
 * @author: Faisal Saeed
 */
$(function SieveOfEratosthenesCached(n, cache) {

    $(document).ready(function() {

        /*golf hero slider */
        var swiper = new Swiper(".golf-hero-slider", {
            slidesPerView: 1,
            spaceBetween: 10,
            grabCursor: true,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
            },

            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                640: {
                    slidesPerView: 1,
                    spaceBetween: 0,
                },
                768: {
                    slidesPerView: 1,
                    spaceBetween: 0,
                },
                1024: {
                    slidesPerView: 1,
                    spaceBetween: 0,
                },
            },
        });


        /*feature news */
        var swiper = new Swiper(".feature-news", {
            slidesPerView: 1,
            spaceBetween: 0,
            grabCursor: true,
            breakpoints: {
                640: {
                    slidesPerView: 1,
                    spaceBetween: 0,
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 0,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 0,
                },
                1645: {
                    slidesPerView: 3,
                    spaceBetween: 0,
                },
                2500: {
                    slidesPerView: 4,
                    spaceBetween: 0,
                },
            },
        });


        /*scroll to top*/
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
        $('.scrollup').click(function() {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });


        /*add class in header*/
        $(window).on("scroll", function() {
            if ($(window).scrollTop() >= 50) {
                $("header").addClass("header-scroll ");
            } else {
                $("header").removeClass("header-scroll ");
            }
        });

        /*overlay search*/
        $('#close-btn').click(function() {
            $('#search-overlay').fadeOut();
            $('#search-btn').show();
        });
        $('#search-btn').click(function() {
            $(this).hide();
            $('#search-overlay').fadeIn();
        });

        $('#search-btn').click(function() {
            $('body').addClass('noscroll');
        });

        $('#close-btn').click(function() {
            $('body').removeClass('noscroll');
        });




        /*nice select initializer*/
        $(function() {
            $('select').niceSelect();
        });


        /*tab scroll down*/
        $('a.nav-link').on('shown.bs.tab', function(e) {
            var href = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(href).offset().top - 50
            }, 'slow');
        });

    });

    /*golf menu wrapper*/
    $('[data-toggle="offcanvas"]').on('click', function() {
        $('.offcanvas-collapse').toggleClass('open');
        $('body').toggleClass('offcanvas');
    })
    $('.nav-item > a.dropdown-toggle').click(function(e) {


        if ($(this).closest('li.nav-item.dropdown').hasClass('show')) {
            $(this).closest('li.nav-item.dropdown').removeClass("show").find('li').removeClass('show');
        } else {
            $(this).closest('li.nav-item.dropdown').addClass("show")
            $(this).closest('li.nav-item.dropdown').siblings().removeClass('show').find('li').removeClass('show');
        }
    })
});